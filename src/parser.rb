require "yaml"
require "sequel"
require "pdf-reader"

require "./src/parser/reader"
require "./src/parser/writer"

def process(filename, config)
  db     = Sequel.connect config["db_url"]
  writer = Parser::Writer.new db
  file   = PDF::Reader.new File.open filename
  pages  = file.pages
  count  = 0

  while (batch = pages.shift config["batches"]).any? do
    result = Parser::Reader.new(batch.map(&:text)).parse
    
    writer.write result
    
    puts result[:district] if count == 0
    print " "
    print count += 1
  end
end

config = YAML.load_file "config/config.yml"

process(ARGV.first, config)
