# coding: utf-8
require "pdf-reader"

require "./src/parser/entry"

module Parser
  class Reader
    @@regexp = /COMUNA: (([\wÑ]+[\s\'])+)/

    def initialize(pages)
      @pages = pages
    end

    def parse
      entries = []
      district = @pages.first.match(@@regexp)
      district = Entry.clean(district.captures.first) unless district.nil?

      @pages.each do |page|
        page.split("\n").each do |line|
          if line =~ /\d+\.\d+\-/
            entries << Entry.parse(line, district)
          end
        end
      end

      ({
         district: district,
         people: entries.map(&:to_h)
       })
    end
  end
end
