require "sequel"

module Parser
  class Writer

    def initialize(db)
      @db = db
    end

    def find_code(district)
      @db
        .from(:district)
        .where(Sequel.lit("name_vec @@ plainto_tsquery('spanish', unaccent(?))", district))
        .all
        .map {|x| x[:code] }
        .first
    end

    def write(result)
      district = find_code(result[:district])
      people = @db.from(:people)

      @db.transaction do
        result[:people].each do |person|
          people
            .insert_conflict
            .insert person.merge({ district: district })
        end
      end
    end
  end
end
