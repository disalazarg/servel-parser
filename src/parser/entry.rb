require "json"

module Parser
  class Entry
    attr_reader :name, :rut, :gender, :address, :circum, :table

    def initialize(name, rut, gender, address, circum, table)
      @name    = name
      @rut     = rut
      @gender  = gender
      @address = address
      @circum  = circum
      @table   = table
    end

    def to_s
      "name: #{name.ljust 50} rut: #{rut.ljust 15} circum: #{circum.ljust 20} table: #{table}"
    end

    def to_h
      {
        name: name,
        rut: rut,
        gender: gender,
        address: address,
        circum: circum,
        mesa: table
      }
    end

    def sanitize(string)
      if string
        string
          .gsub(/'/, '')
      end
    end

    # Module
    def self.parse(line, district=nil)
      params  = line.split(/\s{2}+/)

      name    = clean params.shift
      table   = params.pop.strip
      circum  = clean params.pop
      rut, g  = params.shift.split
      gender  = g == "VAR" ? 'm' : (g == "MUJ" ? 'f' : nil)
      address = clean params.pop if params.any?

      # Obvious rule patch
      if address.nil? and circum.length > 20 and circum.index(district || "non-existent")
        regexp  = Regexp.new(district + '\Z', Regexp::IGNORECASE)
        address = circum.gsub(regexp, "").strip
        circum  = district
      end

      Entry.new name, rut, gender, address, circum, table
    end

    def self.clean(str)
      str
        .split
        .map(&:capitalize)
        .join(" ")
    end
  end
end
