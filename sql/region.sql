INSERT INTO region(code, name, lat, lng) VALUES
(15, 'Arica y Parinacota', -18.5075, -69.6451),
(1, 'Tarapacá', -20.164, -69.5541),
(2, 'Antofagasta', -23.7503, -69.6),
(3, 'Atacama', -27.5276, -70.2494),
(4, 'Coquimbo', -30.8301, -70.9816),
(5, 'Valparaíso', -32.9039, -71.0262),
(13, 'Metropolitana de Santiago', -33.4417, -70.6541),
(6, 'Del Libertador Gral. Bernardo O’Higgins', -34.4294, -71.0393),
(7, 'Del Maule', -35.5892, -71.5007),
(8, 'Del Biobío', -37.2442, -72.4661),
(9, 'De la Araucanía', -38.5505, -72.4382),
(14, 'De los Ríos', -39.9086, -72.7034),
(10, 'De los Lagos', -42.1071, -72.6425),
(11, 'Aysén del Gral. Carlos Ibáñez del Campo', -46.2772, -73.6628),
(12, 'Magallanes y de la Antártica Chilena', -54.3551, -70.5284),
(16, 'Ñuble', -36.6191, -72.0182)
ON CONFLICT DO NOTHING;

UPDATE region SET name_vec = to_tsvector('spanish', unaccent(name)) WHERE name_vec IS NULL;
