CREATE EXTENSION IF NOT EXISTS unaccent;

CREATE TABLE IF NOT EXISTS region(
  code int primary key,
  name varchar,
  name_vec tsvector,
  lat real,
  lng real
);

CREATE TABLE IF NOT EXISTS province(
  code int primary key,
  name varchar,
  name_vec tsvector,
  lat real,
  lng real,
  region int references region(code)
);

CREATE TABLE IF NOT EXISTS district(
  code int primary key,
  name varchar,
  name_vec tsvector,
  lat real,
  lng real,
  province int references province(code),
  region int references region(code)
);

CREATE TABLE IF NOT EXISTS people(
  rut varchar(12) primary key,
  name varchar,
  name_vec tsvector,
  gender varchar(1),
  address varchar,
  circum varchar,
  mesa varchar(6),
  district int references district(code)
);

create index if not exists region_code_idx on region(code);
create index if not exists region_name_idx on region(name_vec);
create index if not exists province_code_idx on province(code, region);
create index if not exists province_name_idx on province(name_vec);
create index if not exists district_code_idx on district(code, province, region);
create index if not exists district_name_idx on district(name_vec);
