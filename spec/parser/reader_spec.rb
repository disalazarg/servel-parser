require "./src/parser/reader"

describe Parser::Reader do
  context "when given a sample page" do
    before do
      @page   = File.read "./spec/fixtures/page.txt"
    end
    
    it "properly parses it and returns a hash with the relevant information" do
      reader = Parser::Reader.new([@page])
      result = reader.parse
      
      expect(result[:district]).to eq "Antartica"
      expect(result[:people]).not_to be_empty
    end
  end

  context "when given an apostrophe on the district" do
    it "can still parse it correctly" do
      reader = Parser::Reader.new(["  COMUNA: O'HIGGINS   "])
      result = reader.parse

      expect(result[:district]).to eq "O'higgins"
    end
  end
end
