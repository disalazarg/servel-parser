# coding: utf-8
require "./src/parser/entry"

describe Parser::Entry do
  context "when given a sample line" do
    before do
      @line = File.read "./spec/fixtures/line.txt"
    end

    it "can parse a sample line into itself" do
      entry = Parser::Entry.parse @line

      expect(entry.name).to eq "Arapio Araya Ricardo Javier"
      expect(entry.gender).to eq "m"
      expect(entry.rut).to eq "10.249.715-5"
      expect(entry.address).to eq "Modulo B/8"
      expect(entry.circum).to eq "Antartica"
      expect(entry.table).to eq "3 V"
    end

    it "can generate a hash form for its data" do
      entry = Parser::Entry.parse @line
      hash  = entry.to_h

      expect(hash).to eq ({
        :name    => "Arapio Araya Ricardo Javier",
        :gender  => "m",
        :rut     => "10.249.715-5",
        :address => "Modulo B/8",
        :circum  => "Antartica",
        :mesa    => "3 V"
      })
    end
  end

  context "when given a long line" do
    it "can deal with it properly, if passed the district" do
      @line = File.read "./spec/fixtures/long.txt"
      entry = Parser::Entry.parse @line, "Alto Hospicio"

      expect(entry.address).not_to be_nil
      expect(entry.address).not_to be_empty
    end

    it "can deal with it, even if there's no space between them" do
      @line = File.read "./spec/fixtures/long_rep.txt"
      entry = Parser::Entry.parse @line.gsub("MAGDALENA ALTO", "MAGDALENAALTO"), "Alto Hospicio"

      expect(entry.address).not_to be_nil
      expect(entry.address).not_to be_empty
      expect(entry.address).to eq "Pje. Alto Hospicio N° 3218 Cond. Santa Magdalena"
      expect(entry.circum).to eq "Alto Hospicio"
    end
  end
end
