# Servel - Parser

Allows parsing of Servel's PDF raw data into a PostgreSQL database.

## Usage

Run with the PDF input file as an argument, for instance:

```bash
$ ruby src/program.rb A1251002.pdf
```

## Config

There's two attributes on `config/config.yml` to be set:

- **db_url**: An URL to pass as an argument to Sequel, such as `postgres://postgres:5432/postgres`.
- **batches**: An integer defining how many pages are processed at once by the Reader before passing it over to the Writer. Defaults to 10.

## Contributors

- [disalazarg](https://github.com/disalazarg) Daniel Salazar - creator, maintainer
